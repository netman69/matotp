#include "sha1.h"
#include "sha256.h"
#include "sha512.h"

#include <stdint.h>
#include <stddef.h>

char base32[256] = { /* RFC 4648 base32 decode lookup table. */
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-1, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, 26, 27, 28, 29, 30, 31, -2, -2, -2, -2, -2, -1, -2, -2,
	-2,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
	15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -2, -2, -2, -2, -2,
	-2,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
	15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
};

size_t base32_parse(uint8_t *out, size_t out_max, char *in) {
	size_t len = 0;
	uint64_t c = 0;
	int i, n = 0;
	while (*in && out_max) {
		int v = base32[(unsigned) *in++];
		if (v < 0) /* NOTE -1 is padding, -2 is error. */
			continue;
		c |= (uint64_t) v << (35 - n * 5);
		if (++n == 8) {
			n = 0;
			for (i = 0; i < 5; ++i) {
				*out++ = c >> (32 - i * 8);
				++len;
				if (out_max-- == 0)
					break;
			}
			c = 0;
		}
	}
	for (i = 0; i < n * 5 / 8 && out_max; ++i) {
		*out++ = c >> (32 - i * 8);
		++len;
		if (out_max-- == 0)
			break;
	}
	return len;
}

/* Note: At most 9 digits make sense for SHA-1. */
void hotp(char *otp, uint8_t *key, size_t key_len, int ndigits,
          uint64_t c) {
	uint8_t dc[8] = {
		c >> 56, c >> 48, c >> 40, c >> 32,
		c >> 24, c >> 16, c >>  8, c >>  0
	};
	uint8_t h[64]; // NOTE change size for hash algo
	sha1_hmac(h, key, key_len, dc, 8);
	int off = h[19] & 0x0F;
	uint32_t hotp = ((h[off + 0] << 24) | (h[off + 1] << 16) |
	                 (h[off + 2] <<  8) | (h[off + 3] <<  0)) &
	                 0x7FFFFFFF;
	int i;
	for (i = ndigits - 1; i >= 0; --i) {
		otp[i] = '0' + hotp % 10;
		hotp /= 10;
	}
	otp[ndigits] = 0;
}

// TODO SHA256 and SHA512
// base32 and hex parsing for secret token

#include <stdio.h>
#include <time.h>

int main(int argc, char *argv[]) {
	//uint8_t h[20];
	int i;
	//sha1(h, argv[1], strlen(argv[1]));
	//sha1_hmac(h, "test", 4, "abcd", 4);
	char otp[10];
	uint8_t key[] = {
		0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x30
	};
	/*for (i = 0; i < 10; ++i) {
		hotp(otp, key, sizeof(key), i);
		printf("%d %s\n", i, otp);
	}*/

	int kl = base32_parse(key, sizeof(key), "e5i7 zxnm yfbf odf4 c2mb f6ox gh4h uoq5 ");
	for (i = 0; i < 10; ++i) {
		hotp(otp, key, kl, 6, time(NULL) / 30 + i);
		printf("%d %s\n", i, otp);
	}

	uint8_t h[64];
	sha512(h, "abc", 3);
	//sha512_hmac(h, "test", 4, "abcd", 4);
	for (i = 0; i < 64; ++i)
		printf("%02x", h[i]);
	printf("\n");
	return 0;
}
