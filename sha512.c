#include "sha512.h"

#include <stdint.h>
#include <stddef.h>
#include <string.h>

inline static uint64_t rdbe64(uint8_t *w) {
	return ((uint64_t) w[0] << 56) |
	       ((uint64_t) w[1] << 48) |
	       ((uint64_t) w[2] << 40) |
	       ((uint64_t) w[3] << 32) |
	       ((uint64_t) w[4] << 24) |
	       ((uint64_t) w[5] << 16) |
	       ((uint64_t) w[6] <<  8) |
	       ((uint64_t) w[7] <<  0);
}

inline static uint64_t ror64(uint64_t a, int b) {
	return (a >> b) | (a << (64 - b));
}

static void sha512_block(sha512_state_t *s) {
	const uint64_t k[80] = {
		0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f,
		0xe9b5dba58189dbbc, 0x3956c25bf348b538, 0x59f111f1b605d019,
		0x923f82a4af194f9b, 0xab1c5ed5da6d8118, 0xd807aa98a3030242,
		0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
		0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235,
		0xc19bf174cf692694, 0xe49b69c19ef14ad2, 0xefbe4786384f25e3,
		0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65, 0x2de92c6f592b0275,
		0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
		0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f,
		0xbf597fc7beef0ee4, 0xc6e00bf33da88fc2, 0xd5a79147930aa725,
		0x06ca6351e003826f, 0x142929670a0e6e70, 0x27b70a8546d22ffc,
		0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
		0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6,
		0x92722c851482353b, 0xa2bfe8a14cf10364, 0xa81a664bbc423001,
		0xc24b8b70d0f89791, 0xc76c51a30654be30, 0xd192e819d6ef5218,
		0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8,
		0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99,
		0x34b0bcb5e19b48a8, 0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb,
		0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3, 0x748f82ee5defb2fc,
		0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
		0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915,
		0xc67178f2e372532b, 0xca273eceea26619c, 0xd186b8c721c0c207,
		0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178, 0x06f067aa72176fba,
		0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b,
		0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc,
		0x431d67c49c100d4c, 0x4cc5d4becb3e42b6, 0x597f299cfc657e2a,
		0x5fcb6fab3ad6faec, 0x6c44198c4a475817
	};
	uint64_t w[80];
	uint64_t wv[8];
	int i;
	
	/* Copy buf contents to w. */
	for (i = 0; i < 128; i += 8)
		w[i / 8] = rdbe64(s->buf + i);
	
	/* Extend block to fill w. */
	for (i = 16; i < 80; ++i)
		w[i] = w[i - 16] + (ror64(w[i - 15], 1) ^ ror64(w[i - 15], 8) ^
		       (w[i - 15] >> 7)) + w[i - 7] + (ror64(w[i - 2], 19) ^
		       ror64(w[i - 2], 61) ^ (w[i - 2] >> 6));
	
	/* Initialize working variables to current hash value. */
	memcpy(wv, s->h, sizeof(uint64_t) * 8);
	
	/* Compression function main loop. */
	for (i = 0; i < 80; ++i) {
		uint64_t t1 = wv[7] + (ror64(wv[4], 14) ^ ror64(wv[4], 18) ^
		              ror64(wv[4], 41)) + ((wv[4] & wv[5]) ^
		              (~wv[4] & wv[6])) + k[i] + w[i];
		uint64_t t2 = (ror64(wv[0], 28) ^ ror64(wv[0], 34) ^
		              ror64(wv[0], 39)) + ((wv[0] & wv[1]) ^
		              (wv[0] & wv[2]) ^ (wv[1] & wv[2]));
		wv[7] = wv[6];
		wv[6] = wv[5];
		wv[5] = wv[4];
		wv[4] = wv[3] + t1;
		wv[3] = wv[2];
		wv[2] = wv[1];
		wv[1] = wv[0];
		wv[0] = t1 + t2;
	}
	
	/* Add the compressed block to the current hash value. */
	for (i = 0; i < 8; ++i)
		s->h[i] += wv[i];
}

void sha512_init(sha512_state_t *s) {
	s->len = 0;
	s->h[0] = 0x6a09e667f3bcc908;
	s->h[1] = 0xbb67ae8584caa73b;
	s->h[2] = 0x3c6ef372fe94f82b;
	s->h[3] = 0xa54ff53a5f1d36f1;
	s->h[4] = 0x510e527fade682d1;
	s->h[5] = 0x9b05688c2b3e6c1f;
	s->h[6] = 0x1f83d9abfb41bd6b;
	s->h[7] = 0x5be0cd19137e2179;
}

void sha512_update(sha512_state_t *s, const uint8_t *msg, size_t len) {
	while (len) {
		int off = s->len % 128;
		int elen = (len > 128 - off) ? 128 - off : len;
		memcpy(s->buf + off, msg, elen);
		s->len += elen;
		msg += elen;
		len -= elen;
		if (s->len % 128 == 0)
			sha512_block(s);
	}
}

void sha512_final(uint8_t h[64], sha512_state_t *s) {
	uint64_t lenb = s->len * 8;
	int i;
	s->buf[s->len++ % 128] = 0x80;
	if (s->len % 128 > 120)
		while (s->len % 128)
			s->buf[s->len++ % 128] = 0;
	if (s->len % 128 == 0)
		sha512_block(s);
	while (s->len % 128 != 120)
		s->buf[s->len++ % 128] = 0;
	s->buf[120] = lenb >> 56;
	s->buf[121] = lenb >> 48;
	s->buf[122] = lenb >> 40;
	s->buf[123] = lenb >> 32;
	s->buf[124] = lenb >> 24;
	s->buf[125] = lenb >> 16;
	s->buf[126] = lenb >>  8;
	s->buf[127] = lenb >>  0;
	sha512_block(s);
	for (i = 0; i < 64; ++i)
		h[i] = s->h[i / 8] >> (56 - i % 8 * 8);
}

void sha512(uint8_t h[64], uint8_t *msg, size_t len) {
	sha512_state_t s;
	sha512_init(&s);
	sha512_update(&s, msg, len);
	sha512_final(h, &s);
}

void sha512_hmac(uint8_t h[64], uint8_t *key, size_t key_len,
                 uint8_t *msg, size_t msg_len) {
	uint8_t dkey_i[128] = { 0 }, dkey_o[128];
	if (key_len > 128)
		sha512((uint8_t *) dkey_i, key, key_len);
	else memcpy((void *) dkey_i, (void *) key, key_len);
	
	int i;
	for (i = 0; i < 128; ++i) {
		dkey_o[i] = dkey_i[i] ^ 0x5C;
		dkey_i[i] ^= 0x36;
	}
	
	sha512_state_t s;
	sha512_init(&s);
	sha512_update(&s, (uint8_t *) dkey_i, 128);
	sha512_update(&s, (uint8_t *) msg, msg_len);
	sha512_final(h, &s);
	sha512_init(&s);
	sha512_update(&s, (uint8_t *) dkey_o, 128);
	sha512_update(&s, h, 64);
	sha512_final(h, &s);
}
