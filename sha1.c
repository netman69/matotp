#include "sha1.h"

#include <stddef.h>
#include <stdint.h>
#include <string.h>

inline static uint32_t rdbe32(uint8_t *w) {
	return ((uint32_t) w[0] << 24) |
	       ((uint32_t) w[1] << 16) |
	       ((uint32_t) w[2] <<  8) |
	       ((uint32_t) w[3] <<  0);
}

inline static uint32_t rol32(uint32_t x, int n) {
	return (x << n) | (x >> (32 - n));
}

static void sha1_block(sha1_state_t *s) {
	uint32_t w[80];
	int i;
	
	for (i = 0; i < 16; ++i)
		w[i] = rdbe32((uint8_t *) s->buf + i * 4);
	
	for (i = 16; i < 80; ++i)
		w[i] = rol32(w[i - 3] ^ w[i - 8] ^ w[i - 14] ^ w[i - 16], 1);
	
	uint32_t a = s->h[0];
	uint32_t b = s->h[1];
	uint32_t c = s->h[2];
	uint32_t d = s->h[3];
	uint32_t e = s->h[4];
	
	for (i = 0; i < 80; ++i) {
		uint32_t f, k;
		if (i < 20) {
			f = (b & c) | ((~b) & d);
			k = 0x5A827999;
		} else if (i < 40) {
			f = b ^ c ^ d;
			k = 0x6ED9EBA1;
		} else if (i < 60) {
			f = (b & c) | (b & d) | (c & d);
			k = 0x8F1BBCDC;
		} else {
			f = b ^ c ^ d;
			k = 0xCA62C1D6;
		}
		uint32_t tmp = rol32(a, 5) + f + e + k + w[i];
		e = d;
		d = c;
		c = rol32(b, 30);
		b = a;
		a = tmp;
	}
	
	s->h[0] += a;
	s->h[1] += b;
	s->h[2] += c;
	s->h[3] += d;
	s->h[4] += e;
}

void sha1_init(sha1_state_t *s) {
	s->h[0] = 0x67452301;
	s->h[1] = 0xEFCDAB89;
	s->h[2] = 0x98BADCFE;
	s->h[3] = 0x10325476;
	s->h[4] = 0xC3D2E1F0;
	s->len = 0;
}

void sha1_update(sha1_state_t *s, uint8_t *msg, size_t len) {
	while (len--) {
		int pos = s->len++ % 64;
		s->buf[pos] = *msg++;
		if (pos == 63)
			sha1_block(s);
	}
}

void sha1_final(uint8_t h[20], sha1_state_t *s) {
	int pos = s->len % 64;
	s->buf[pos++] = 0x80;
	if (pos > 56) {
		while (pos < 64)
			s->buf[pos++] = 0;
		sha1_block(s);
		pos = 0;
	}
	while (pos < 56)
		s->buf[pos++] = 0;
	uint64_t ml = s->len * 8;
	s->buf[pos++] = ml >> 56;
	s->buf[pos++] = ml >> 48;
	s->buf[pos++] = ml >> 40;
	s->buf[pos++] = ml >> 32;
	s->buf[pos++] = ml >> 24;
	s->buf[pos++] = ml >> 16;
	s->buf[pos++] = ml >> 8;
	s->buf[pos++] = ml >> 0;
	sha1_block(s);
	int i;
	for (i = 0; i < 5; ++i) {
		h[i * 4 + 0] = s->h[i] >> 24;
		h[i * 4 + 1] = s->h[i] >> 16;
		h[i * 4 + 2] = s->h[i] >> 8;
		h[i * 4 + 3] = s->h[i] >> 0;
	}
}

void sha1(uint8_t h[20], uint8_t *msg, size_t len) {
	sha1_state_t s;
	sha1_init(&s);
	sha1_update(&s, msg, len);
	sha1_final(h, &s);
}

void sha1_hmac(uint8_t h[20], uint8_t *key, size_t key_len,
               uint8_t *msg, size_t msg_len) {
	uint8_t dkey_i[64] = { 0 }, dkey_o[64];
	if (key_len > 64)
		sha1((uint8_t *) dkey_i, key, key_len);
	else memcpy((void *) dkey_i, (void *) key, key_len);
	
	int i;
	for (i = 0; i < 64; ++i) {
		dkey_o[i] = dkey_i[i] ^ 0x5C;
		dkey_i[i] ^= 0x36;
	}
	
	sha1_state_t s;
	sha1_init(&s);
	sha1_update(&s, (uint8_t *) dkey_i, 64);
	sha1_update(&s, (uint8_t *) msg, msg_len);
	sha1_final(h, &s);
	sha1_init(&s);
	sha1_update(&s, (uint8_t *) dkey_o, 64);
	sha1_update(&s, h, 20);
	sha1_final(h, &s);
}
