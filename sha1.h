#ifndef __SHA1_H__
#define __SHA1_H__

#include <stddef.h>
#include <stdint.h>

typedef struct {
	uint32_t h[5];
	uint64_t len;
	uint8_t buf[64];
} sha1_state_t;

extern void sha1_init(sha1_state_t *s);
extern void sha1_update(sha1_state_t *s, uint8_t *msg, size_t len);
extern void sha1_final(uint8_t h[20], sha1_state_t *s);
extern void sha1(uint8_t h[20], uint8_t *msg, size_t len);
extern void sha1_hmac(uint8_t h[20], uint8_t *key, size_t key_len,
                      uint8_t *msg, size_t msg_len);

#endif /* __SHA1_H__ */
