CFLAGS += -Wall -pedantic
OBJS = main.o sha1.o sha256.o sha512.o

all: matotp

matotp: $(OBJS)
	$(CC) -o $@ $(LDFLAGS) $(OBJS)

clean:
	rm -f matotp $(OBJS)
