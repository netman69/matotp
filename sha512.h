#ifndef __SHA512_H__
#define __SHA512_H__

#include <stdint.h>
#include <stddef.h>

typedef struct {
	uint64_t h[8];
	uint8_t buf[128];
	uint64_t len;
} sha512_state_t;

extern void sha512_init(sha512_state_t *s);
extern void sha512_update(sha512_state_t *s, const uint8_t *msg,
                          size_t len);
extern void sha512_final(uint8_t h[64], sha512_state_t *s);
extern void sha512(uint8_t h[64], uint8_t *msg, size_t len);
extern void sha512_hmac(uint8_t h[64], uint8_t *key, size_t key_len,
                        uint8_t *msg, size_t msg_len);

#endif /* __SHA512_H__ */
